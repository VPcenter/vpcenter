#include <stdio.h>
#include <stdbool.h>

/*********************************************************************************************************** 
*   Класс pwFunc, в структуре которого с модификаторами доступа 'во всей программе' (public) и             *
*   'в рамках класса' (private) объявлены переменные для пользовательской (математической) функции;        *
*   статический одномерный массив arr на 10 элементов; метод для запроса на ввод переменных enterQuery();  *
*   метод вычисления пользовательской функции eval(); метод сортировки "пузырьком" bubbleSort();           *
*   метод для вывода значений массива printArr().                                                          *
*   В структуре главной функции main() находится объявление переменной smFunc типа pwFunc. В строках 60-63 *
*   осуществляется последовательный вызов функций для ввода значений переменных функции, её вычисления,    *
*   соритровки и вывода.                                                                                   *
************************************************************************************************************/

class pwFunc{
    public:
        float a, b, c, f;

        void enterQuery(){
            printf("Enter variable 'a': "); scanf("%f", &a);
            printf("Enter variable 'b': "); scanf("%f", &b);
            printf("Enter variable 'c': "); scanf("%f", &c);
        }
        
        void eval(float x, float y, float z){
            for (int i = 0; i < 10; i++){
                arr[i] = (x + y + z) * i;
                printf("%.1f ", arr[i]);
            }
        }

        void bubbleSort(){
            float temp;
            bool ex;

            do{
                ex = false;
                for (int i = 0; i < 9; i++){
                    if (arr[i] < arr[i + 1]){
                        temp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = temp;
                        ex = true;
                    }
                }
            }while(ex);
        }
        
        void printArr(){
            printf("\n");
            for (int i = 0; i < 10; i++)
                printf("%.1f ", arr[i]);
        }   
        
    private:
        float arr[10];
};

int main(){
    pwFunc smFunc;
    
        smFunc.enterQuery();
        smFunc.eval(smFunc.a, smFunc.b, smFunc.c);
        smFunc.bubbleSort();
        smFunc.printArr();
    
    return 0;
}